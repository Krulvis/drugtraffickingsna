setwd("drugtraffickingsna")
#please set your working directory to the repo folder

#### Operation Chalonero ----

# Import data
chalonero_nodes <- read.table("2.data/Chalonero_table.csv", header=TRUE, sep=',')
chalonero_edges <- read.table("2.data/Chalonero_edgelist.csv", header=TRUE, sep=',')

chalonero_graph <- igraph::graph_from_data_frame(d = chalonero_edges, 
                                                 directed = FALSE, 
                                                 vertices = chalonero_nodes)

chalonero_network <- intergraph::asNetwork(chalonero_graph)
colors=c(1,2,3)
names(colors)=c("Red", "Blue", "Orange")
colors[c("Red","Blue","Blue","Orange")]


# Network nice visualization

unique_tasks <- unique(igraph::V(chalonero_graph)$Task)
colrs <- c("gray50", "tomato", "gold", "orange", "cadetblue")
names(colrs) <- unique_tasks
colrs[igraph::V(chalonero_graph)$Task]

igraph::V(chalonero_graph)$color <- colrs[igraph::V(chalonero_graph)$Task] # we attribute task type to color
igraph::E(chalonero_graph)$arrow.size <- .2
igraph::E(chalonero_graph)$edge.color <- "gray80"

# Run only one of the next:
V(chalonero_graph)$size = degree(chalonero_graph, mode = "all")/2
igraph::V(chalonero_graph)$size = igraph::betweenness(chalonero_graph)/10
V(chalonero_graph)$size = closeness(chalonero_graph)/.001

plot(chalonero_graph,
     vertex.frame.color = "#ffffff",#node perimeter
     vertex.label = igraph::V(chalonero_graph)$name, 
     vertex.label.cex = 0.6,  # vertex label size
     vertex.label.color = "black"
)
graphics::legend(x = -1.5, y = -1.1, unique_tasks, pch = 21,
                 col = "#777777", pt.bg = colrs, pt.cex = 2, cex = .8, bty = "n", ncol = 1)
# Explore network

igraph::V(chalonero_graph) 

igraph::ecount(chalonero_graph) # (136)

igraph::diameter(chalonero_graph) # longest shortest path in the network (5)
igraph::mean_distance(chalonero_graph) # average distance between each pair of nodes (2.36)
igraph::edge_density(chalonero_graph, loops=TRUE)  # ratio of the number of edges and the number of possible edges (0.17)

igraph::degree(chalonero_graph, v = igraph::V(chalonero_graph), mode = "all", loops = TRUE, normalized = FALSE) # degree of each node
igraph::degree(chalonero_graph, v = igraph::V(chalonero_graph), mode = "all", loops = TRUE, normalized = TRUE) # degree of each node NORMALIZED
igraph::assortativity_degree(chalonero_graph) # nodes' preference to attach to other nodes based on the degree (-0.30) NEGATIVE == nodes high degree connect with nodes low degree
igraph::transitivity(chalonero_graph, type = "global") # overall probability for the network to have adjacent nodes interconnected (0.24)

# STATISTICAL ANALYSIS (ERGMs)

prob_out_of_coefs <- function(x) {
  return(exp(x)/ (1 + exp(x)))
}

# Model edges in the network are not random
chalonero_model.01 <- ergm::ergm(chalonero_network ~ edges)  

summary(chalonero_model.01) # Negative coef (-2.05) indicates sparseness
summary(chalonero_model.01)$coefficients[1] %>% prob_out_of_coefs() #Probability of edge formation (0.113)

# Is it task involved in propensity of ties generation? 
chalonero_model.02 <- ergm::ergm(chalonero_network ~ edges + nodefactor('Task'))  #Likelihood to form ties with Traffickers
summary(chalonero_model.02)
summary(chalonero_model.02)$coefficients[1] %>% prob_out_of_coefs() #Probability to form ties with Trafficker (0.87)

# Is there more or less degree 2 nodes than expected?
chalonero_model.03 <- ergm::ergm(chalonero_network ~ edges + degree(2))  #Likelihood to form ties with 2 other nodes
summary(chalonero_model.03)
summary(chalonero_model.03)$coefficients[1] %>% prob_out_of_coefs() #Probability to form ties with 2 other nodes (0.83)

# Is there homophily in the model?
chalonero_model.04 <- ergm::ergm(chalonero_network ~ edges + nodematch('Task'))  #Likelihood to form ties with similar people
summary(chalonero_model.04) # Not significant
summary(chalonero_model.04)$coefficients[1] %>% prob_out_of_coefs()

## Operation Stupor Mundi ----------

stupor_mundi_nodes<-read.table("2.data/StuporMundi_table.csv", header=TRUE, sep=',')
stupor_mundi_edges <- read.table("2.data/StuporMundi_edgelist.csv", header=TRUE, sep=',')

stupor_mundi_graph <- igraph::graph_from_data_frame(d = stupor_mundi_edges, 
                                                    directed = FALSE, 
                                                    vertices = stupor_mundi_nodes)

stupor_mundi_graph <- igraph::simplify(stupor_mundi_graph, remove.multiple = F, remove.loops = T) 
stupor_mundi_network <- intergraph::asNetwork(stupor_mundi_graph)

colors=c(1,2,3)
names(colors)=c("Red", "Blue", "Orange")
colors[c("Red","Blue","Blue","Orange")]


# Network nice visualization

unique_tasks <- unique(igraph::V(stupor_mundi_graph)$Task)
colrs <- c("gray50", "tomato", "gold", "orange", "cadetblue")
names(colrs) <- unique_tasks
colrs[igraph::V(stupor_mundi_graph)$Task]

igraph::V(stupor_mundi_graph)$color <- colrs[igraph::V(stupor_mundi_graph)$Task] # we attribute task type to color
igraph::E(stupor_mundi_graph)$arrow.size <- .2
igraph::E(stupor_mundi_graph)$edge.color <- "gray80"

# Run only one of the next:
V(stupor_mundi_graph)$size = degree(stupor_mundi_graph, mode = "all")/2
V(stupor_mundi_graph)$size = betweenness(stupor_mundi_graph, mode = "all")/10
V(stupor_mundi_graph)$size = closeness(stupor_mundi_graph, mode = "all")/.001

# plot GRAPH
plot(stupor_mundi_graph,
     vertex.frame.color = "#ffffff",#node perimeter
     vertex.label = igraph::V(stupor_mundi_graph)$name, 
     vertex.label.cex = 0.6,  # vertex label size
     vertex.label.color = "black"
)

graphics::legend(x = -1.5, y = -1.1, unique_tasks, pch = 21,
                 col = "#777777", pt.bg = colrs, pt.cex = 2, cex = .8, bty = "n", ncol = 1)

# Explore network
igraph::V(stupor_mundi_graph) 

igraph::ecount(stupor_mundi_graph) #(314)

igraph::diameter(stupor_mundi_graph) # longest shortest path in the network (5)
igraph::mean_distance(stupor_mundi_graph) # average distance between each pair of nodes (2.07)
igraph::edge_density(stupor_mundi_graph, loops=TRUE)  # ratio of the number of edges and the number of possible edges (0.26)

igraph::degree(stupor_mundi_graph, v = igraph::V(stupor_mundi_graph), mode = "all", loops = TRUE, normalized = FALSE) # degree of each node
igraph::degree(stupor_mundi_graph, v = igraph::V(stupor_mundi_graph), mode = "all", loops = TRUE, normalized = TRUE) # degree of each node NORMALIZED
igraph::assortativity_degree(stupor_mundi_graph) # nodes' preference to attach to other nodes based on the degree (-0.31) NEGATIVE == nodes high degree connect with nodes low degree
igraph::transitivity(stupor_mundi_graph, type = "global") # overall probability for the network to have adjacent nodes interconnected (0.29)


# STATISTICAL ANALYSIS (ERGMs)

prob_out_of_coefs <- function(x) {
  return(exp(x)/ (1 + exp(x)))
}

# Model edges in the network are not random
supor_mundi_model.01 <- ergm::ergm(stupor_mundi_network ~ edges)  

summary(supor_mundi_model.01) # Negative coef (-1.8) indicates sparseness
summary(supor_mundi_model.01)$coefficients[1] %>% prob_out_of_coefs() #Probability of edge formation (0.14)

# Is it task involved in propensity of ties generation? 
supor_mundi_model.02 <- ergm::ergm(stupor_mundi_network ~ edges + nodefactor('Task'))  #Likelihood to form ties with Traffickers
summary(supor_mundi_model.02)
summary(supor_mundi_model.02)$coefficients[1] %>% prob_out_of_coefs() #Probability to form ties with Trafficker (0.76)

# Is there more or less degree 3 nodes than expected?
supor_mundi_model.03 <- ergm::ergm(stupor_mundi_network ~ edges + degree(3))  #Likelihood to form ties with 3 other nodes
summary(supor_mundi_model.03)
summary(supor_mundi_model.03)$coefficients[1] %>% prob_out_of_coefs() #Probability to form ties with 3 other nodes (0.89)

# Is there homophily in the model?
supor_mundi_model.04 <- ergm::ergm(stupor_mundi_network ~ edges + nodematch('Task'))  #Likelihood to form ties with similar people
summary(supor_mundi_model.04)
summary(supor_mundi_model.04)$coefficients[1] %>% prob_out_of_coefs() #Probability to form ties with similar people (0.68)
