# 1. Dataset creation 
*Data files already exist in Repo*
 - There's two networks that we generate based on images and tables found in research papers
 - Start with both Python Notebooks to generate the edge/node list of both networks
 - To load and visualize the networks run `BuildingNetworks.R`
# 2. Data
 - Directory that contains the data used in our research
## 3.1 First exploratory analysis
 - Base descriptive measures for each network combined with plots of the network.
##3.2 Network descriptors
 - Additional network descriptors
## 3.3 Plots
 - Directory with image- and pdf files containing descriptive plots and graphs
##4.1 CUGTest
- R file with code for the CUG Test
## 4.2 CUG Results
 - Directory with images of CUG test results
##5.1 ERGM
 - R File containing code for ERG Models
##5.2 ERGM Results
 - Directory with images of ERGM result, GOF & MCMC plots

 